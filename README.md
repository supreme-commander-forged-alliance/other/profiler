# Profiler

A profiler made for the game Supreme Commander: Forged Alliance. It can keep track how often functions in files and classes are called. It contains a series of benchmarks that can help you get an idea of when code is more efficient. These benchmarks are run in-game to make them as representable as possible.

## Hooking non-class files

These are the typical lua files with a series of function definitions inside. An example is `/lua/ScenarioFramework.lua`. They are relative easy to hook. All you need to do is add the files you want to hook to the [configuration file](/mods/profiler/config.lua) in a similar fashion as those that are already in there.

## Hooking class files

These are class-based files such as `/lua/sim/unit.lua`. They are hard to hook reliably. You need access to the game files and add code to each function individually. It is best to have a local repository such as the [FAF]() or [LOUD]() repository and make the changes on a separate branch. The changes required are described in [this file](/mods/profiler/profiling-classes.lua). There are regular expressions to help you out. Please note that removing the added code is a harder, therefore working on a separate branch is advised.

## Todo

 - [ ] Implement count stamps
 - [ ] Implement timers
 - [ ] Allow the global table to be hooked

## Sources

Icon is made by Freepik on on [favicon.cc](https://www.flaticon.com/free-icon/statistics_2920349?term=statistics&page=1&position=6&page=1&position=6&related_id=2920349&origin=search).