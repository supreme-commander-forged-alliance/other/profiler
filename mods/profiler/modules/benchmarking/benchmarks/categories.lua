
-- Cached:  21 ms
-- Inline:  130 ms 

-- AddCheckBase:    56 ms
-- AddCheck:        75 ms
-- AddCheckOpti:    68 ms

-- The performance of the cached version is faster when there is more than one
-- inner loop, but typically (significantly) better than the inline version. 
-- Highly recommending to upvalue the categories computation in both class 
-- and table based files.

local outerLoop = 1000
local innerLoop = 100

function Cached()

    local unit = CreateUnit("uaa0303", 1, 0, 0, 0, 0, 0, 0, 0)

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    for k = 1, outerLoop do 
        local cached = (categories.AIR * categories.TECH3) + categories.EXPERIMENTAL
        for l = 1, innerLoop do 
            EntityCategoryContains(cached, unit)
        end
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    unit:Destroy()

    return final - start
end

function Inline()

    local unit = CreateUnit("uaa0303", 1, 0, 0, 0, 0, 0, 0, 0)
    
    local start = GetSystemTimeSecondsOnlyForProfileUse()

    for k = 1, outerLoop do 
        for l = 1, innerLoop do 
            EntityCategoryContains((categories.AIR * categories.TECH3) + categories.EXPERIMENTAL, unit)
        end
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    unit:Destroy()

    return final - start
end

-- The following functions replicate this code:

-- UpdateAssistersConsumption = function(self)
--     local units = {}
--     -- We need to check all the units assisting.
--     for _, v in self:GetGuards() do
--         if not v.Dead and (v:IsUnitState('Building') or v:IsUnitState('Repairing')) and not (EntityCategoryContains(categories.INSIGNIFICANTUNIT, v)) then
--             table.insert(units, v)
--         end
--     end

--     (...)
-- end

function AddCheckBase()

    local unit = CreateUnit("uaa0303", 1, 0, 0, 0, 0, 0, 0, 0)
    
    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local isNotDead = false 
    local isBuildingOrRepairing = false 
    -- local isNotInsignificant = false 

    for k = 1, outerLoop do 
        for l = 1, innerLoop do 
            isNotDead = (not unit.Dead)
            isBuildingOrRepairing = (unit:IsUnitState('Building') or unit:IsUnitState('Repairing'))
            -- isNotInsignificant = not (EntityCategoryContains(categories.INSIGNIFICANTUNIT, v))
        end
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    unit:Destroy()

    return final - start
end

function AddCheck()

    local unit = CreateUnit("uaa0303", 1, 0, 0, 0, 0, 0, 0, 0)
    
    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local isNotDead = false 
    local isBuildingOrRepairing = false 
    local isNotInsignificant = false 

    for k = 1, outerLoop do 
        for l = 1, innerLoop do 
            isNotDead = (not unit.Dead)
            isBuildingOrRepairing = (unit:IsUnitState('Building') or unit:IsUnitState('Repairing'))
            isNotInsignificant = not (EntityCategoryContains(categories.INSIGNIFICANTUNIT, unit))
        end
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    unit:Destroy()

    return final - start
end

-- upvalue for performance
local CategoriesInsignificantUnit = categories.INSIGNIFICANTUNIT
local EntityCategoryContains = EntityCategoryContains
local UnitIsUnitState = _G.moho.unit_methods.IsUnitState

function AddCheckOpti()

    local unit = CreateUnit("uaa0303", 1, 0, 0, 0, 0, 0, 0, 0)
    
    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local isNotDead = false 
    local isBuildingOrRepairing = false 
    local isNotInsignificant = false 

    for k = 1, outerLoop do 
        for l = 1, innerLoop do 
            isNotDead = (not unit.Dead)
            isBuildingOrRepairing = (UnitIsUnitState(unit, 'Building') or UnitIsUnitState(unit, 'Repairing'))
            isNotInsignificant = not EntityCategoryContains(CategoriesInsignificantUnit, unit)
        end
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    unit:Destroy()

    return final - start
end