
-- BVDist3:         10.70 ms
-- BVDist3Sq:       10.25 ms

-- BVDist2:         6.83 ms
-- BVDist2Sq:       6.34 ms

-- BVDist2SqStack:  4.38 ms
-- BVDist2SqManual: 1.96 ms
-- BVDist2Manual:   4.12 ms

-- Conclusion: engine calls are expensive! But I'm not sure if this optimalisation
-- is worth it in practice - perhaps when it is computed each time in a loop construct.

local VDist3 = VDist3
local VDist3Sq = VDist3Sq
local VDist2 = VDist2
local VDist2Sq = VDist2Sq 

local v1 = { 1, 1, 1 }
local v2 = { 4, 4, 4 }

function BVDist3()

    local v1 = v1 
    local v2 = v2

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local dis = 0
    for k = 1, 100000 do 
        dis = VDist3(v1, v2)
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

local TableGetn = table.getn

function BVDist3Sq()

    local v1 = v1 
    local v2 = v2

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local dis = 0
    for k = 1, 100000 do 
        dis = VDist3Sq(v1, v2)
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

function BVDist2()
    local v1 = v1 
    local v2 = v2

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local dis = 0
    for k = 1, 100000 do 
        dis = VDist2(v1[1], v1[3], v2[1], v2[3])
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

function BVDist2Sq()
    local v1 = v1 
    local v2 = v2

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local dis = 0
    for k = 1, 100000 do 
        dis = VDist2Sq(v1[1], v1[3], v2[1], v2[3])
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

function BVDist2SqStack()
    local x1 = v1[1]
    local z1 = v1[3]
    local x2 = v2[1]
    local z2 = v2[3]

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local dis = 0
    for k = 1, 100000 do 
        dis = VDist2Sq(x1, z1, x2, z2)
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

function BVDist2SqManual()
    local x1 = v1[1]
    local z1 = v1[3]
    local x2 = v2[1]
    local z2 = v2[3]

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local dis = 0
    for k = 1, 100000 do 
        local x = x2 - x1
        local z = z2 - z1
        dis = x * x + z * z
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

local MathSqrt = math.sqrt

function BVDist2Manual()
    local x1 = v1[1]
    local z1 = v1[3]
    local x2 = v2[1]
    local z2 = v2[3]

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local dis = 0
    for k = 1, 100000 do 
        local x = x2 - x1
        local z = z2 - z1
        dis = MathSqrt(x * x + z * z)
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end