

-- EngineCall:  326 ms
-- Cached:      55 ms

local outerLoop = 1000000

function EngineCall()

    -- create a dummy unit
    local unit = CreateUnit("uaa0303", 1, 0, 0, 0, 0, 0, 0, 0)

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local count = false
    local weapon = false
    for k = 1, outerLoop do 
        count = unit:GetWeaponCount()
        for k = 1, count do 
            weapon = unit:GetWeapon(k)
        end
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    -- remove dummy unit
    unit:Destroy()

    return final - start
end

function Cached()

    -- create a dummy unit
    local unit = CreateUnit("ues0103", 1, 0, 0, 0, 0, 0, 0, 0)

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    -- cache once
    unit.Weapons = { }
    unit.WeaponCount = unit:GetWeaponCount()
    for k = 1, unit.WeaponCount do 
        unit.Weapons[k] = unit:GetWeapon(k)
    end

    -- use indefinitely
    local count = false
    local weapon = false
    for k = 1, outerLoop do 
        count = unit.WeaponCount
        for k = 1, count do 
            weapon = unit.Weapons[k]
        end
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    -- remove dummy unit
    unit:Destroy()

    return final - start
end