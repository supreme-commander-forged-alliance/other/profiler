
-- populate dummy data
local data = { }
for k = 1, 1000 do 
    data[k] = k 
end

-- typical approach
local TableGetn = table.getn

function TableGetNUpvalue()

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local count = 0
    for k = 1, 100000 do 
        count = TableGetn(data)
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

-- binary approach

-- local function FindLimits(table)
--     local index = 1
--     local entry = table[index]

--     while entry do 
--         index = index * 2 
--         entry = table[index]
--     end

--     return 0.5 * index, index
-- end

-- local MathFloor = math.floor

-- local function FindLimit(table, lower, upper)

--     local mid = false
--     while lower ~= upper + 1 do 

--         mid = MathFloor(lower + 0.5 * (upper - lower))
--         if table[mid] then 
--             lower = mid 
--         else 
--             upper = mid 
--         end
--     end
    
--     return lower

    
-- end

-- function TableGetNBinary()

--     local start = GetSystemTimeSecondsOnlyForProfileUse()

--     local count = 0
--     for k = 1, 100000 do 
--         local lower, upper = FindLimits(data)
--         count = FindLimit(data, lower, upper)
--     end

--     LOG(count)
    
--     local final = GetSystemTimeSecondsOnlyForProfileUse()

--     return final - start
-- end