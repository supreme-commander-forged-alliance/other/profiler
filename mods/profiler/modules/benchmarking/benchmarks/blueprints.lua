

-- EngineCall:          199 ms
-- TableRetrieval:      16.1 ms
-- TableRetrievalCheat: 10.2 ms
-- TableRetrievalSafe:  19.3 ms
-- TableStored:         9.25 ms

local outerLoop = 1000000

-- upvalue is better
local __blueprints = __blueprints

function EngineCall()

    -- create a dummy unit
    local unit = CreateUnit("uaa0303", 1, 0, 0, 0, 0, 0, 0, 0)

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local blueprint = false
    for k = 1, outerLoop do 
        blueprint = unit:GetBlueprint()
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    -- remove dummy unit
    unit:Destroy()

    return final - start
end

function TableRetrieval()

    -- create a dummy unit
    local unit = CreateUnit("uaa0303", 1, 0, 0, 0, 0, 0, 0, 0)
    unit.BpId = "uaa0303"

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local blueprint = false
    for k = 1, outerLoop do 
        blueprint = __blueprints[unit.BpId]
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    -- remove dummy unit
    unit:Destroy()

    return final - start
end

function TableRetrievalCheat()

    -- create a dummy unit
    local unit = CreateUnit("uaa0303", 1, 0, 0, 0, 0, 0, 0, 0)

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local blueprint = false
    for k = 1, outerLoop do 
        blueprint = __blueprints["uaa0303"]
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    -- remove dummy unit
    unit:Destroy()

    return final - start
end

function TableRetrievalSafe()

    -- create a dummy unit
    local unit = CreateUnit("uaa0303", 1, 0, 0, 0, 0, 0, 0, 0)
    unit.BpId = "uaa0303"

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local blueprint = false
    for k = 1, outerLoop do 
        blueprint = __blueprints[unit.BpId] or unit:GetBlueprint()
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    -- remove dummy unit
    unit:Destroy()

    return final - start
end

function TableStored()

    -- create a dummy unit
    local unit = CreateUnit("uaa0303", 1, 0, 0, 0, 0, 0, 0, 0)
    unit.Blueprint = unit:GetBlueprint()

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local blueprint = false
    for k = 1, outerLoop do 
        blueprint = unit.Blueprint
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    -- remove dummy unit
    unit:Destroy()

    return final - start
end