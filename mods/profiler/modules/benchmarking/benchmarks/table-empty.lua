
-- TableEmptyGlobal:    44.32 ms
-- TableEmptyUpvalue:   42.85 ms

-- TableGetNGlobal:     45.93 ms
-- TableGetNUpvalue:    45.42 ms

-- TableIndex:          1.983 ms

-- Conclusion: preventing table operations (the '.') is quite the benefit in practice.

function TableGetNGlobal()

    local data = { }

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local empty = false
    for k = 1, 100000 do 
        empty = table.getn(data) > 0
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

local TableGetn = table.getn

function TableGetNUpvalue()

    local data = { }

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local empty = false
    for k = 1, 100000 do 
        empty = TableGetn(data) > 0
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

function TableEmptyGlobal()

    local data = { }

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local empty = false
    for k = 1, 100000 do 
        empty = table.empty(data)
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

local TableEmpty = table.empty

function TableEmptyUpvalue()

    local data = { }

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local empty = false
    for k = 1, 100000 do 
        empty = TableEmpty(data)
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

function TableIndex()
    local data = { }

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local empty = false
    for k = 1, 100000 do 
        empty = data[1] ~= nil
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end