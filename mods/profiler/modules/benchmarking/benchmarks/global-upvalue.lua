
-- Global   28ms
-- Upvalue: 23ms ~ 24ms
-- Local:   19ms ~ 20ms

-- GlobalSingleton: 26ms
-- UpvalueSingleton: 23ms ~ 24ms
-- LocalSingleton: 19ms ~ 20ms

-- GlobalValue: 17ms
-- UpvalueValue: 14ms
-- LocalValue: 10ms

local outerLoop = 1000000

-- Adds all elements of a (large) table

-- global scoped data
globalData = { }
for k = 1, outerLoop do 
    globalData[k] = 1
end
function Global()

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local sum = 0
    for k = 1, outerLoop do 
        sum = sum + globalData[k]
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

-- upvalue scoped data
local upData = { }
for k = 1, outerLoop do 
    upData[k] = 1
end
function Upvalue()

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local sum = 0
    for k = 1, outerLoop do 
        sum = sum + upData[k]
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

-- local scoped
local localData = { }
for k = 1, outerLoop do 
    localData[k] = 1
end
function Local()

    local data = localData

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local sum = 0
    for k = 1, outerLoop do 
        sum = sum + data[k]
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

-- Adds the first entry of a table many times

-- global scoped
globalDataSingleton = { 1 }
function GlobalSingleton()

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local sum = 0
    for k = 1, outerLoop do 
        sum = sum + globalDataSingleton[1]
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

-- upvalue scoped
local upDataSingleton = { 1 } 
function UpvalueSingleton()

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local sum = 0
    for k = 1, outerLoop do 
        sum = sum + upDataSingleton[1]
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

-- local scoped
local localDataSingleton = { 1 } 
function LocalSingleton()

    local data = localDataSingleton

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local sum = 0
    for k = 1, outerLoop do 
        sum = sum + data[1]
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end


-- Adds a single value many times

-- global scoped
globalDataValue = 1
function GlobalValue()

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local sum = 0
    for k = 1, outerLoop do 
        sum = sum + globalDataValue
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

-- upvalue scoped
local upDataValue = 1 
function UpvalueValue()

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local sum = 0
    for k = 1, outerLoop do 
        sum = sum + upDataValue
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

-- local scoped
local localDataValue = 1 
function LocalValue()

    local data = localDataValue

    local start = GetSystemTimeSecondsOnlyForProfileUse()

    local sum = 0
    for k = 1, outerLoop do 
        sum = sum + data
    end

    local final = GetSystemTimeSecondsOnlyForProfileUse()

    return final - start
end

-- lua byte code

-- -- Global
-- Code={
--     "(  25)    0 - GETGLOBAL      0    0",
--     "(  25)    1 - CALL           0    1    2",
--     "(  27)    2 - LOADK          1    1",
--     "(  28)    3 - LOADK          2    2",
--     "(  28)    4 - GETUPVAL       3    0    0",
--     "(  28)    5 - LOADK          4    2",
--     "(  28)    6 - SUB            2    2    4",
--     "(  28)    7 - JMP            0    3",
--     "(  29)    8 - GETGLOBAL      5    3",      --< note this global get call
--     "(  29)    9 - GETTABLE       5    5    2",
--     "(  29)   10 - ADD            1    1    5",
--     "(  28)   11 - FORLOOP        2   -4",
--     "(  32)   12 - GETGLOBAL      2    0",
--     "(  32)   13 - CALL           2    1    2",
--     "(  34)   14 - SUB            3    2    0",
--     "(  34)   15 - RETURN         3    2    0",
--     "(  35)   16 - RETURN         0    1    0"
-- },

-- -- Upvalue
-- Code={
--     "(  44)    0 - GETGLOBAL      0    0",
--     "(  44)    1 - CALL           0    1    2",
--     "(  46)    2 - LOADK          1    1",
--     "(  47)    3 - LOADK          2    2",
--     "(  47)    4 - GETUPVAL       3    0    0",
--     "(  47)    5 - LOADK          4    2",
--     "(  47)    6 - SUB            2    2    4",
--     "(  47)    7 - JMP            0    3",
--     "(  48)    8 - GETUPVAL       5    1    0", --< note this upvalue get call
--     "(  48)    9 - GETTABLE       5    5    2",
--     "(  48)   10 - ADD            1    1    5",
--     "(  47)   11 - FORLOOP        2   -4",
--     "(  51)   12 - GETGLOBAL      2    0",
--     "(  51)   13 - CALL           2    1    2",
--     "(  53)   14 - SUB            3    2    0",
--     "(  53)   15 - RETURN         3    2    0",
--     "(  54)   16 - RETURN         0    1    0"
-- },

-- -- Local
-- Code={
--     "(  63)    0 - GETUPVAL       0    0    0",
--     "(  65)    1 - GETGLOBAL      1    0",
--     "(  65)    2 - CALL           1    1    2",
--     "(  67)    3 - LOADK          2    1",
--     "(  68)    4 - LOADK          3    2",
--     "(  68)    5 - GETUPVAL       4    1    0",
--     "(  68)    6 - LOADK          5    2",
--     "(  68)    7 - SUB            3    3    5",
--     "(  68)    8 - JMP            0    2",
--                                                 --< no upvalue / global get call
--     "(  69)    9 - GETTABLE       6    0    3",
--     "(  69)   10 - ADD            2    2    6",
--     "(  68)   11 - FORLOOP        3   -3",
--     "(  72)   12 - GETGLOBAL      3    0",
--     "(  72)   13 - CALL           3    1    2",
--     "(  74)   14 - SUB            4    3    1",
--     "(  74)   15 - RETURN         4    2    0",
--     "(  75)   16 - RETURN         0    1    0"
-- },

